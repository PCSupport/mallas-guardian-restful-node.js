var mysql = require('mysql');

function Connection() {
  this.pool = null;

  this.init = function() {
    this.pool = mysql.createPool({
      connectionLimit: 40,
      host: 'www.db4free.net',
      user: 'pernett98',
      password: '123456',
      database: 'mallas',
      chartset: 'UTF8_GENERAL_CI'
    });
  };

  this.acquire = function(callback) {
    this.pool.getConnection(function(err, connection) {
      callback(err, connection);
    });
  };
}

module.exports = new Connection();
