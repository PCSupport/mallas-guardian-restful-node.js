var connection = require('../../config/connection');

function User() {
  this.get = function(res) {
    connection.acquire(function(err, con) {
      if(con){
        con.query('select * from USUARIOS', function(err, result) {
          con.release();
          res.send(result);
        });
      };
    });
  };

  this.create = function(user, res) {
    connection.acquire(function(err, con) {
      if(con){
        con.query('insert into USUARIOS set ?', user, function(err, result) {
          con.release();
          if (err) {
            res.send({status: 1, message: 'USER creation failed', res: err});
          } else {
            res.send({status: 0, message: 'USER created successfully'});
          }
        });
      };
    });
  };


  this.update = function(user, res) {
    connection.acquire(function(err, con) {
      if(con){
        con.query('update USUARIOS set ? where USUARIO = ?', [user, user.USUARIO], function(err, result) {
          con.release();
          if (err) {
            res.send({status: 1, message: 'USER update failed', err: err});
          } else {
            res.send({status: 0, message: 'USER updated successfully'});
          }
        });
      };
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      if(con){
        con.query('delete from USUARIOS where USUARIO = ?', [id], function(err, result) {
          con.release();
          if (err) {
            res.send({status: 1, message: 'Failed to delete', err: err});
          } else {
            res.send({status: 0, message: 'Deleted successfully'});
          }
        });
      };
    });
  };
};


module.exports = new User();
