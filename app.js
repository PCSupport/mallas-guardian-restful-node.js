var express = require('express');
var bodyparser = require('body-parser');
var morgan = require('morgan');
var passport = require('passport');
var connection = require('./config/connection');
var routes = require('./routes');
var port = process.env.PORT || 8000;

var app = express();

// get our request parameters
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})

// log to console
app.use(morgan('dev'));

// Use the passport package in our application
app.use(passport.initialize());

connection.init();
routes.configure(app);

var server = app.listen(port, function() {
  console.log('Server listening on port ' + server.address().port);
});
